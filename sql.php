<!DOCTYPE html>
<head><meta charset="utf-8"/></head>

<?php

try {
    $db = new PDO(
        "mysql:host=127.0.0.1;dbname=web;charset=utf8",
        "root",
        "root",
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
    );
} catch (Exception $e) {
    die("Error : " . $e->getMessage());
}

$console = "NES";

$games = $db->prepare("SELECT nom FROM jeux_video WHERE console=? ORDER BY nom ASC");
$games->execute(array($console));

if (isset($games)) {
    echo("Jeux possédés sur Nes : <br/> <ul>");

    while ($jeu = $games->fetch()) {
        echo("<li>" . $jeu["nom"] . "</li>");
    }

    echo("</ul>");
}

$games->closeCursor();